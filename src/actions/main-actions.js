import axios from "axios";
import axiosRetry from "axios-retry";
import {
  FETCH_WEATHER,
  FETCH_WEATHER_ERROR,
  FETCH_WEATHER_SUCCESS
} from "../constants/action-types";

const apiKey = "660795ddd1853ae4c769b1572057ca3c";
const CancelToken = axios.CancelToken;
export let cancel;

axiosRetry(axios, { retries: 3 });

const getWeatherService = city => {
  const URL = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`;
  return axios.get(URL, {
    cancelToken: new CancelToken(function executor(c) {
      // An executor function receives a cancel function as a parameter
      cancel = c;
    })
  });
};

const fetchWeather = () => ({
  type: FETCH_WEATHER
});

const fetchWeatherSuccess = weather => ({
  type: FETCH_WEATHER_SUCCESS,
  payload: weather
});

const fetchWeatherFails = err => ({
  type: FETCH_WEATHER_ERROR,
  message: err.message,
  payload: err
});

export const getWeatherFromCity = city => dispatch => {
  dispatch(fetchWeather());
  return getWeatherService(city)
    .then(res =>
      setTimeout(() => {
        dispatch(fetchWeatherSuccess(res.data));
        // dispatch(fetchWeatherFails({ message: "Something went wrong" }));
      }, 5000)
    )
    .catch(() => {
      dispatch(fetchWeatherFails({ message: "Something went wrong" }));
    });
};
