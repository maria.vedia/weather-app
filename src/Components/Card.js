import React from "react";
import "../css/Card.css";

const Card = ({ icon, temp, pressure, humidity }) => {
  const iconUrl = `http://openweathermap.org/img/w/${icon}.png`;
  const img = icon ? (
    <img className="icon" src={iconUrl} alt="Weather icon" />
  ) : (
    undefined
  );
  return (
    <div>
      {img}
      <h3>{`Temperatura: ${temp}`}</h3>
      <h3>{`Presión: ${pressure}`}</h3>
      <h3>{`Humedad: ${humidity}`}</h3>
    </div>
  );
};

export default Card;
