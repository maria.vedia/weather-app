import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getWeatherFromCity, cancel } from "../actions/main-actions";
import Card from "./Card";
import Spinner from "./Spinner";

class CardList extends Component {
  componentWillUnmount() {
    if (cancel) {
      cancel();
    }
  }

  loadWeather = city => {
    const { loadingWeather, getWeatherService } = this.props;
    if (cancel && loadingWeather) {
      cancel();
    }
    getWeatherService(city);
  };

  render() {
    const { weather, errorMessage, loadingWeather } = this.props;
    const errorScreen = <p>Error: {errorMessage}</p>;
    console.log({ weather });

    return (
      <Fragment>
        <div>
          <button type="button" onClick={() => this.loadWeather("Madrid")}>
            Madrid
          </button>
          <button type="button" onClick={() => this.loadWeather("London")}>
            Londres
          </button>
          <button type="button" onClick={() => this.loadWeather("Roma")}>
            Roma
          </button>
        </div>

        <div>
          {loadingWeather && <Spinner />}
          {errorMessage && errorScreen}
          {weather && (
            <Card
              icon={weather.weather[0].icon}
              temp={weather.main.temp}
              pressure={weather.main.pressure}
              humidity={weather.main.humidity}
            />
          )}
        </div>
      </Fragment>
    );
  }
}

CardList.defaultProps = {
  weather: undefined,
  errorMessage: "Something went wrong",
  loadingWeather: true,
  getWeatherService: () => {}
};

CardList.propTypes = {
  weather: PropTypes.shape({
    weather: PropTypes.array,
    main: PropTypes.shape({
      temp: PropTypes.number,
      pressure: PropTypes.number,
      humidity: PropTypes.number
    })
  }),
  errorMessage: PropTypes.string,
  loadingWeather: PropTypes.bool,
  getWeatherService: PropTypes.func
};

const mapStateToProps = state => ({
  weather: state.weather,
  loadingWeather: state.loading,
  errorMessage: state.errorMessage,
  error: state.error
});

const mapDispatchToProps = dispatch => {
  return {
    getWeatherService: city => {
      dispatch(getWeatherFromCity(city));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CardList);
