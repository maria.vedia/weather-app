import {
  FETCH_WEATHER,
  FETCH_WEATHER_ERROR,
  FETCH_WEATHER_SUCCESS
} from "../constants/action-types";

const initialState = {
  weather: null,
  loading: true,
  errorMessage: null
};
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_WEATHER:
      return {
        ...state,
        loading: true,
        weather: null,
        errorMessage: null
      };
    case FETCH_WEATHER_SUCCESS:
      console.log(action);
      return {
        ...state,
        loading: false,
        weather: action.payload,
        errorMessage: null
      };
    case FETCH_WEATHER_ERROR:
      console.log(action);
      return {
        ...state,
        loading: false,
        errorMessage: action.message
      };
    default:
      return state;
  }
};

export default mainReducer;
