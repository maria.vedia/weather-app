import mainReducer from "./main.reducer";
import {
  FETCH_WEATHER,
  FETCH_WEATHER_SUCCESS,
  FETCH_WEATHER_ERROR
} from "../constants/action-types";

describe("main_reducer", () => {
  describe("INITIAL_STATE", () => {
    test("is correct", () => {
      const action = { type: "initial_action" };
      expect(mainReducer(undefined, action)).toMatchSnapshot();
    });
  });

  describe("FETCH_WEATHER", () => {
    test("returns the correct fetch weather state", () => {
      const action = { type: FETCH_WEATHER };
      expect(mainReducer(undefined, action)).toMatchSnapshot();
    });
  });

  describe("FETCH_WEATHER_SUCCESS", () => {
    test("returns the correct fetch weather success state", () => {
      const action = { type: FETCH_WEATHER_SUCCESS };
      expect(mainReducer(undefined, action)).toMatchSnapshot();
    });
  });

  describe("FETCH_WEATHER_ERROR", () => {
    test("returns the correct fetch weather error state", () => {
      const action = { type: FETCH_WEATHER_ERROR };
      expect(mainReducer(undefined, action)).toMatchSnapshot();
    });
  });
});
