import React, { Component, Fragment } from "react";
import CardList from "../Components/CardList";

class Home extends Component {
  render() {
    return (
      <Fragment>
        <CardList />
      </Fragment>
    );
  }
}

export default Home;
